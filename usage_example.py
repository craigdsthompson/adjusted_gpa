import adjusted_gpa
import itertools
import matplotlib.pyplot as plt
import pandas as pd
import pprint


def run(filename):
    '''

    :param filename:
    :return:
    '''
    df = pd.read_csv(filename)
    df['Course'] = df['Course'].astype(str)
    print("Full dataset contains {} Courses and {} Students".format(len(df["Course"].unique()), len(df["Student"].unique())))
    components = adjusted_gpa.check_connectedness(df, raise_error=False)
    if components is None:
        process_df(df)
    else:
        i = 0
        for item in components:
            reduced_df = df[df["Course"].isin(list(item))]
            print("Processing component {} which has {} courses and {} students.".format(i, len(reduced_df["Course"].unique()), len(reduced_df["Student"].unique())))
            process_df(reduced_df)
            i += 1


def process_df(df):
    largest_course = df.groupby('Course')['Student'].count().idxmax()

    a_students, a_courses = adjusted_gpa.additive_method(df)
    a_fit = adjusted_gpa.check_additive_fit(df, a_students, a_courses)
    reconstructed_additive = adjusted_gpa.create_additive_data(a_students, a_courses, df[['Student', 'Course']].values)[
        0]
    scaled_a_students, scaled_a_courses = adjusted_gpa.scale_additive_results(a_students, a_courses, largest_course, 0)
    print('Additive fit:', a_fit)
    print('Additive Course parameters:')
    pprint.pprint(scaled_a_courses)

    m_students, m_courses = adjusted_gpa.multiplicative_method(df)
    m_fit = adjusted_gpa.check_multiplicative_fit(df, m_students, m_courses)
    reconstructed_multiplicative = \
        adjusted_gpa.create_multiplicative_data(m_students, m_courses, df[['Student', 'Course']].values)[0]
    scaled_m_students, scaled_m_courses = adjusted_gpa.scale_multiplicative_results(m_students, m_courses,
                                                                                    largest_course, 1)
    print('Multiplicative fit:', m_fit)
    print('Multiplicative Course parameters:')
    pprint.pprint(scaled_m_courses)

    c_students, c_courses = adjusted_gpa.combined_method(df)
    c_fit = adjusted_gpa.check_combined_fit(df, c_students, c_courses)
    reconstructed_combined = adjusted_gpa.create_combined_data(c_students, c_courses, df[['Student', 'Course']].values)[
        0]
    scaled_c_students, scaled_c_courses = adjusted_gpa.scale_combined_results(c_students, c_courses, largest_course,
                                                                              {'a': 0, 'm': 1})
    print('Combined fit:', c_fit)
    print('Combined Course parameters:')
    pprint.pprint(scaled_c_courses)

    simple_students = dict(df.groupby('Student')['Grade'].mean())
    simple_courses = dict(zip(df['Course'].unique(), itertools.repeat(1)))
    simple_avg_fit = adjusted_gpa.check_multiplicative_fit(df, simple_students, simple_courses)
    reconstructed_simple_avg = \
        adjusted_gpa.create_multiplicative_data(simple_students, simple_courses, df[['Student', 'Course']].values)[0]

    print('Simple Average fit:', simple_avg_fit)

    plt.figure(1)
    plt.scatter(df['Grade'], reconstructed_simple_avg['Grade'])
    plt.xlim(0, 100)
    plt.ylim(0, 100)
    plt.title("Simple Average Method RMSE: {}".format(simple_avg_fit))
    plt.xlabel('Actual Grade')
    plt.ylabel('Reconstructed grade')

    plt.figure(2)
    plt.scatter(df['Grade'], reconstructed_additive['Grade'])
    plt.xlim(0, 100)
    plt.ylim(0, 100)
    plt.title("Additive Method RMSE: {}".format(a_fit))
    plt.xlabel('Actual Grade')
    plt.ylabel('Reconstructed grade')

    plt.figure(3)
    plt.scatter(df['Grade'], reconstructed_multiplicative['Grade'])
    plt.xlim(0, 100)
    plt.ylim(0, 100)
    plt.title("Multiplicative Method RMSE: {}".format(m_fit))
    plt.xlabel('Actual Grade')
    plt.ylabel('Reconstructed grade')

    plt.figure(4)
    plt.scatter(df['Grade'], reconstructed_combined['Grade'])
    plt.xlim(0, 100)
    plt.ylim(0, 100)
    plt.title("Combined Method RMSE: {}".format(c_fit))
    plt.xlabel('Actual Grade')
    plt.ylabel('Reconstructed grade')
    plt.show()


if __name__ == "__main__":
    run('test_dataset/data.csv')
