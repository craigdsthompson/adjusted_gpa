from setuptools import setup, find_packages
from codecs import open
from os import path

here = path.abspath(path.dirname(__file__))

# Get the long description from the README file
with open(path.join(here, 'README.rst'), encoding='utf-8') as f:
    long_description = f.read()

setup(
    name='adjusted_gpa',
    version='0.0.1',
    description='A python project for computing course difficulty adjusted GPAs',
    long_description=long_description,
    url='https://bitbucket.org/craigdsthompson/adjusted_gpa',
    author='Craig Thompson',
    author_email='craigdsthompson@gmail.com',
    license='MIT',
    classifiers=[
        'Development Status :: 3 - Alpha',
        'Intended Audience :: Developers',
        'License :: OSI Approved :: MIT License',
        'Programming Language :: Python :: 3.4',
    ],
    keywords='course difficulty adjusted grading',
    packages=find_packages(exclude=['contrib', 'docs', 'tests']),
    install_requires=['pandas','scipy'],
    tests_require=['nose']
)