import collections
import itertools
import numpy as np
import operator
import pandas as pd
import scipy.optimize
import scipy.sparse


def RMSE(arr1, arr2):
    return np.sqrt(np.mean(abs(arr1-arr2)**2))


def check_additive_fit(df, students, courses):
    new_df = df.copy()
    new_df['Grade'] = new_df['Student'].map(students) - new_df['Course'].map(courses)
    return RMSE(df['Grade'], new_df['Grade'])


def check_multiplicative_fit(df, students, courses):
    new_df = df.copy()
    new_df['Grade'] = new_df['Student'].map(students) / new_df['Course'].map(courses)
    return RMSE(df['Grade'], new_df['Grade'])


def check_combined_fit(df, students, courses):
    new_df = df.copy()
    new_df['Grade'] = (new_df['Student'].map(students) / new_df['Course'].map(lambda x: courses[x]['m'])) - new_df['Course'].map(lambda x: courses[x]['a'])
    return RMSE(df['Grade'], new_df['Grade'])


def get_current_value(students, courses, target):
    if target in students and target in courses:
        raise RuntimeError("student and course with same identifier")
    elif target in students:
        current_value = students[target]
    elif target in courses:
        current_value = courses[target]
    else:
        raise RuntimeError("No student or course found with given identifier")
    return current_value


def scale_additive_results(students, courses, target, desired_value):

    current_value = get_current_value(students, courses, target)

    difference = desired_value - current_value
    new_students = {}
    for k in students:
        new_students[k] = students[k]+difference
    new_courses = {}
    for k in courses:
        new_courses[k] = courses[k]+difference
    return new_students, new_courses


def scale_multiplicative_results(students, courses, target, desired_value):

    current_value = get_current_value(students, courses, target)

    difference = desired_value / current_value
    new_students = {}
    for k in students:
        new_students[k] = students[k]*difference
    new_courses = {}
    for k in courses:
        new_courses[k] = courses[k]*difference
    return new_students, new_courses


def scale_combined_results(students, courses, target, desired_value):

    current_value = get_current_value(students, courses, target)
    additive_difference = desired_value['a'] - current_value['a']
    multiplicative_difference = desired_value['m'] / current_value['m']

    # do multiplicative difference:
    new_students = {}
    for k in students:
        new_students[k] = students[k]*multiplicative_difference
    new_courses = {}
    for k in courses:
        new_courses[k] = {}
        new_courses[k]['m'] = courses[k]['m']*multiplicative_difference

    # do additive difference
    new_courses[target]['a'] = courses[target]['a']+additive_difference
    for k in students:
        new_students[k] = new_students[k]+(additive_difference*new_courses[target]['m'])
    for k in set(courses.keys()).difference(set([target])):
        new_courses[k]['a'] = courses[k]['a']+(additive_difference*new_courses[target]['m']/new_courses[k]['m'])

    return new_students, new_courses


def create_dataframe(students, courses, score_function, enrollments=None):
    if enrollments is None:
        enrollments = itertools.product(sorted(students), sorted(courses))
    all_grades = [(s, c, score_function(students[s], courses[c])) for (s, c) in enrollments]
    df = pd.DataFrame(all_grades, columns=['Student', 'Course', 'Grade'])
    return df


def create_additive_data(students, courses, enrollments=None):
    students_and_courses = students.copy()
    students_and_courses.update(courses)
    df = create_dataframe(students, courses, operator.sub, enrollments)
    return df, students_and_courses


def create_multiplicative_data(students, courses, enrollments=None):
    students_and_courses = students.copy()
    students_and_courses.update(courses)
    df = create_dataframe(students, courses, operator.truediv, enrollments)
    return df, students_and_courses


def create_combined_data(students, courses, enrollments=None):
    all_factors = students.copy()
    for c in courses:
        all_factors.update({c+'_'+k: v for k, v in courses[c].items()})
    df = create_dataframe(students, courses, lambda x, y: (x/y['m'])-y['a'], enrollments)
    return df, all_factors


def enumerate_students_and_courses(df):
    student_ixs = {student_id: N for N, student_id in enumerate(sorted(df['Student'].unique()))}
    course_ixs = {course_id: N for N, course_id in enumerate(sorted(df['Course'].unique()))}

    s = df['Student'].replace(student_ixs)
    c = df['Course'].replace(course_ixs)
    students_and_courses = np.column_stack((s.values, c.values))

    grades = df['Grade'].values

    identifiers = list(sorted(student_ixs.keys())) + list(sorted(course_ixs.keys()))
    return student_ixs, course_ixs, students_and_courses, grades, identifiers


def check_connectedness(df, raise_error=True):
    # This function will raise an exception of the course enrollments in the dataframe
    # do not form one connected component.
    course_idx = {course_id: N for N, course_id in enumerate(sorted(df['Course'].unique()))}
    course_idx_rev = {idx: name for name, idx in course_idx.items()}
    max_idx = max(course_idx.values())

    # replace course names with indices
    df['Course_number'] = df['Course'].map(course_idx)

    # create course connectedness matrix
    result = df.groupby('Student').apply(lambda x: itertools.product(x['Course_number'].values, repeat=2))
    flattened = [r for re in result for r in re]
    counts = collections.Counter(flattened)
    locs, vals = zip(*counts.items())
    connectedness_mat = scipy.sparse.csr_matrix((vals, np.array(locs).T), shape=(max_idx+1, max_idx+1))

    # undo adding course indices
    df.drop('Course_number', axis=1, inplace=True)

    # check connected components
    n_components, component_labels = scipy.sparse.csgraph.connected_components(connectedness_mat, directed=False)
    if n_components != 1:
        d = collections.defaultdict(list)
        for course_idx, component_number in enumerate(component_labels):
            d[component_number].append(course_idx_rev[course_idx])
        err_msg = ("Dataset must contain only one connected component.\n"
                   "Delete data from the source file corresponding to all but one of the connected components.\n"
                   "Number of connected components: {}".format(n_components))
        for component_number, members in d.items():
            err_msg = err_msg+"\nCourses in component {}: {}".format(component_number, members)
        if raise_error:
            raise RuntimeError(err_msg)
        return d.values()
    return None


def make_additive_indicator_matrix_and_grades(df):
    # Note: the additive indicator matrix is under-determined.
    # The relative value of the students' adjusted gpa
    # and the course difficulty indicies is not fixed.
    # To solve this problem, we fix the course difficulty
    # of a single course to be 0. This ensures that a unique,
    # repeatable solution can be found.
    student_ixs, course_ixs, students_and_courses, grades, identifiers = enumerate_students_and_courses(df)
    students_and_courses[:, 1] += len(student_ixs)
    row = np.concatenate((np.tile(np.arange(len(grades)), (2,)), [len(grades)]))
    column = np.concatenate((students_and_courses.ravel('F'), [students_and_courses[:, 1].min()]))
    data = np.concatenate((np.tile((1, -1), (len(grades), 1)).ravel('F'), [1]))
    indicator_matrix = scipy.sparse.csr_matrix((data, (row, column)), shape=(len(grades) + 1, len(identifiers)))
    grades = np.concatenate((df['Grade'].values, [0]))
    return indicator_matrix, grades, sorted(student_ixs.keys()), sorted(course_ixs.keys())


def make_multiplicative_indicator_matrix_and_grades(df):
    student_ixs, course_ixs, students_and_courses, grades, identifiers = enumerate_students_and_courses(df)
    student_guesses = df.groupby('Student').mean().to_dict()['Grade']
    initial_guess = [student_guesses[s] for s in sorted(student_ixs.keys())]
    initial_guess = initial_guess + [1]*len(course_ixs)
    students_and_courses[:, 1] += len(student_ixs)
    return initial_guess, grades, students_and_courses, sorted(student_ixs.keys()), sorted(course_ixs.keys())


def make_combined_indicator_matrix_and_grades(df):
    student_ixs, course_ixs, students_and_courses, grades, identifiers = enumerate_students_and_courses(df)
    student_guesses = df.groupby('Student').mean().to_dict()['Grade']
    initial_guess = [student_guesses[s] for s in sorted(student_ixs.keys())]
    initial_guess = initial_guess + [0]*len(course_ixs) + [1]*len(course_ixs)
    students_and_courses[:, 1] += len(student_ixs)
    students_and_courses = np.concatenate((students_and_courses, (students_and_courses[:, 1] + len(course_ixs))[:, np.newaxis]), axis=1)
    return initial_guess, grades, students_and_courses, sorted(student_ixs.keys()), sorted(course_ixs.keys())


def additive_method(df):
    ind, grades, students, courses = make_additive_indicator_matrix_and_grades(df)
    results = scipy.sparse.linalg.lsqr(ind, grades)[0]
    num_students = len(students)
    students = dict(zip(students, results[:num_students]))
    courses = dict(zip(courses, results[num_students:]))
    return students, courses


def multiplicative_func(x, y_data, s_c):
    # x is a guess at the parameters
    # y_data is the true data
    # s_c is a list of student-course pairs corresponding to ydata
    residual = y_data - x[s_c[:, 0]]/x[s_c[:, 1]]
    return residual


def multiplicative_grad(x, y_data, s_c):
    data = np.zeros(2*y_data.shape[0])
    data[np.arange(y_data.shape[0])] = -1/x[s_c[:, 1]]
    data[y_data.shape[0]+np.arange(y_data.shape[0])] = x[s_c[:, 0]]/(x[s_c[:, 1]])**2
    row_ind = np.tile(np.arange(y_data.shape[0]), 2)
    col_ind = np.concatenate((s_c[:, 0], s_c[:, 1]))
    jacobian = scipy.sparse.csr_matrix((data, (row_ind, col_ind)), shape=(y_data.shape[0], len(x)))
    return jacobian


def multiplicative_method(df):
    initial_guess, grades, students_and_courses, students, courses = make_multiplicative_indicator_matrix_and_grades(df)
    results = scipy.optimize.least_squares(multiplicative_func, initial_guess, jac=multiplicative_grad, args=(grades, students_and_courses))
    num_students = len(students)
    students = dict(zip(students, results['x'][:num_students]))
    courses = dict(zip(courses, results['x'][num_students:]))
    return students, courses


def combined_func(x, y_data, s_c):
    # x is a guess at the parameters
    # y_data is the true data
    # s_c is a list of student-course pairs corresponding to ydata
    residual = y_data - (x[s_c[:, 0]]/x[s_c[:, 2]] - x[s_c[:, 1]])
    return residual


def combined_grad(x, y_data, s_c):
    data = np.zeros(3*y_data.shape[0])
    data[np.arange(y_data.shape[0])] = -1/x[s_c[:, 2]]
    data[y_data.shape[0]+np.arange(y_data.shape[0])] = 1
    data[2*y_data.shape[0]+np.arange(y_data.shape[0])] = x[s_c[:, 0]]/(x[s_c[:, 2]])**2
    row_ind = np.tile(np.arange(y_data.shape[0]), 3)
    col_ind = np.concatenate((s_c[:, 0], s_c[:, 1], s_c[:, 2]))
    jacobian = scipy.sparse.csr_matrix((data, (row_ind, col_ind)), shape=(y_data.shape[0], len(x)))
    return jacobian


def combined_method(df):
    initial_guess, grades, students_and_courses, students, courses = make_combined_indicator_matrix_and_grades(df)
    results = scipy.optimize.least_squares(combined_func, initial_guess, jac=combined_grad, args=(grades, students_and_courses))
    num_students = len(students)
    num_courses = len(courses)
    students = dict(zip(students, results['x'][:num_students]))
    additive = results['x'][num_students:num_students+num_courses]
    multiplicative = results['x'][-num_courses:]
    courses = {c: {'a': a, 'm': m} for c, a, m in zip(courses, additive, multiplicative)}
    return students, courses
