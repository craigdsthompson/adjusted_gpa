import adjusted_gpa
import nose
import numpy as np
import pandas as pd


# Helper functions used for testing
def validate_dicts(d1, d2):
    nose.tools.eq_(d1.keys(), d2.keys())

    array1 = np.array([v for k, v in sorted(d1.items())])
    array2 = np.array([v for k, v in sorted(d2.items())])

    np.testing.assert_array_almost_equal(array1, array2, err_msg="dict1 = {}\ndict2 = {}".format(d1, d2))


def validate_combined_method_course_dicts(d1, d2):
    nose.tools.eq_(d1.keys(), d2.keys())

    for k in d1.keys():
        validate_dicts(d1[k], d2[k])


# Test cases
def test_sample_datafile():
    # ensure that the contents of the datafile match the expectation from the Caulkins paper

    df = pd.read_csv('caulkins.csv')
    assert (len(df) == 20)
    assert (df[df['Student'] == 'Student A']['Grade'].mean() == 88.60)
    assert (df[df['Student'] == 'Student B']['Grade'].mean() == 89.20)
    assert (df[df['Student'] == 'Student C']['Grade'].mean() == 89.60)
    assert (df[df['Student'] == 'Student D']['Grade'].mean() == 90.60)
    assert (df[df['Course'] == 'Class #1']['Grade'].mean() == 91.50)
    assert (df[df['Course'] == 'Class #2']['Grade'].mean() == 82.50)
    assert (df[df['Course'] == 'Class #3']['Grade'].mean() == 97.50)
    assert (df[df['Course'] == 'Class #4']['Grade'].mean() == 77.50)
    assert (df[df['Course'] == 'Class #5']['Grade'].mean() == 96.00)
    assert (df[df['Course'] == 'Class #6']['Grade'].mean() == 89.00)
    assert (df[df['Course'] == 'Class #7']['Grade'].mean() == 90.50)
    assert (df[df['Course'] == 'Class #8']['Grade'].mean() == 90.25)
    assert (df[df['Course'] == 'Class #9']['Grade'].mean() == 90.00)


def test_make_additive_indicator_matrix_and_grades():
    df = pd.read_csv('caulkins.csv')

    ind, grades, students, courses = adjusted_gpa.make_additive_indicator_matrix_and_grades(df)

    expected_ind = np.array(
        [[1, 0, 0, 0, -1, 0, 0, 0, 0, 0, 0, 0, 0],
         [1, 0, 0, 0, 0, -1, 0, 0, 0, 0, 0, 0, 0],
         [1, 0, 0, 0, 0, 0, 0, -1, 0, 0, 0, 0, 0],
         [1, 0, 0, 0, 0, 0, 0, 0, 0, -1, 0, 0, 0],
         [1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, -1, 0],
         [0, 1, 0, 0, 0, 0, 0, -1, 0, 0, 0, 0, 0],
         [0, 1, 0, 0, 0, 0, 0, 0, -1, 0, 0, 0, 0],
         [0, 1, 0, 0, 0, 0, 0, 0, 0, 0, -1, 0, 0],
         [0, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, -1, 0],
         [0, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, -1],
         [0, 0, 1, 0, 0, -1, 0, 0, 0, 0, 0, 0, 0],
         [0, 0, 1, 0, 0, 0, -1, 0, 0, 0, 0, 0, 0],
         [0, 0, 1, 0, 0, 0, 0, 0, 0, 0, -1, 0, 0],
         [0, 0, 1, 0, 0, 0, 0, 0, 0, 0, 0, -1, 0],
         [0, 0, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, -1],
         [0, 0, 0, 1, -1, 0, 0, 0, 0, 0, 0, 0, 0],
         [0, 0, 0, 1, 0, 0, -1, 0, 0, 0, 0, 0, 0],
         [0, 0, 0, 1, 0, 0, 0, 0, -1, 0, 0, 0, 0],
         [0, 0, 0, 1, 0, 0, 0, 0, 0, -1, 0, 0, 0],
         [0, 0, 0, 1, 0, 0, 0, 0, 0, 0, 0, -1, 0],
         [0, 0, 0, 0, 1, 0, 0, 0, 0, 0, 0, 0, 0]])
    np.testing.assert_array_equal(ind.toarray(), expected_ind)

    expected_grades = np.array([93, 85, 80, 93, 92, 75, 97, 92, 91, 91, 80, 100, 89, 90, 89, 90, 95, 95, 85, 88, 0])
    assert ((grades == expected_grades).all())

    expected_students = ['Student A', 'Student B', 'Student C', 'Student D']
    expected_courses = ['Class #1', 'Class #2', 'Class #3', 'Class #4',
                        'Class #5', 'Class #6', 'Class #7', 'Class #8',
                        'Class #9']
    assert(students == expected_students)
    assert(courses == expected_courses)


def test_caulkins_additive_solution():
    # Caulkins does not state exact adjusted GPA scores,
    # but that the adjusted rank ordering should be A>B>C>D (top of page 2)
    df = pd.read_csv('caulkins.csv')
    students, courses = adjusted_gpa.additive_method(df)
    assert (students['Student A'] > students['Student B'] > students['Student C'] > students['Student D'])


def test_additive_method():
    # test the additive method, using fake data created additively.
    # we should be able to reverse-engineer these student ability scores,
    # and course difficulty scores

    students = {'s1': 70}
    courses = {'c1': 0}
    df, students_and_courses = adjusted_gpa.create_additive_data(students, courses)
    result_students, result_courses = adjusted_gpa.additive_method(df)
    validate_dicts(students, result_students)
    validate_dicts(courses, result_courses)

    students = {'s1': 70, 's2': 60}
    courses = {'c1': 0}
    df, students_and_courses = adjusted_gpa.create_additive_data(students, courses)
    result_students, result_courses = adjusted_gpa.additive_method(df)
    validate_dicts(students, result_students)
    validate_dicts(courses, result_courses)

    students = {'s1': 80}
    courses = {'c1': 0, 'c2': 5}
    df, students_and_courses = adjusted_gpa.create_additive_data(students, courses)
    result_students, result_courses = adjusted_gpa.additive_method(df)
    validate_dicts(students, result_students)
    validate_dicts(courses, result_courses)

    students = {'s1': 62, 's2': 79}
    courses = {'c1': 0, 'c2': -10}
    df, students_and_courses = adjusted_gpa.create_additive_data(students, courses)
    result_students, result_courses = adjusted_gpa.additive_method(df)
    validate_dicts(students, result_students)
    validate_dicts(courses, result_courses)

    # Three students in three courses, but every student
    # is missing from one of the courses
    students = {'s1': 62, 's2': 79, 's3': 81}
    courses = {'c1': 0, 'c2': -10, 'c3': 5}
    enrollments = [('s1', 'c2'), ('s1', 'c3'),
                   ('s2', 'c1'), ('s2', 'c3'),
                   ('s3', 'c1'), ('s3', 'c2')]
    df, students_and_courses = adjusted_gpa.create_additive_data(students, courses, enrollments)
    result_students, result_courses = adjusted_gpa.additive_method(df)
    validate_dicts(students, result_students)
    validate_dicts(courses, result_courses)


def test_additive_method_noisy_data():

    # Three students in three courses
    students = {'s1': 62, 's2': 79, 's3': 81}
    courses = {'c1': 0, 'c2': -10, 'c3': 5}
    original_df, students_and_courses = adjusted_gpa.create_additive_data(students, courses)

    # Add noise to the dataset
    noisy_df = original_df.copy()
    noisy_df.loc[(noisy_df['Student'] == 's1') & (noisy_df['Course'] == 'c1'), 'Grade'] += 3
    noisy_df.loc[(noisy_df['Student'] == 's2') & (noisy_df['Course'] == 'c3'), 'Grade'] -= 4

    students, courses = adjusted_gpa.additive_method(noisy_df)
    new_df, _ = adjusted_gpa.create_additive_data(students, courses)

    np.testing.assert_almost_equal(courses['c1'], 0)

    # The RMSE between the noisy_df and the new_df should be smaller than between the noisy_df and original_df
    RMSE_original = adjusted_gpa.RMSE(original_df['Grade'], noisy_df['Grade'])
    RMSE_new = adjusted_gpa.RMSE(new_df['Grade'], noisy_df['Grade'])
    nose.tools.ok_(RMSE_new <= RMSE_original)


def test_additive_method_random_data():
    # Even with random data, the additive factor for the first class should be zero
    students = {'s1': 62, 's2': 79, 's3': 81}
    courses = {'c1': 0, 'c2': -10, 'c3': 5}
    df, students_and_courses = adjusted_gpa.create_additive_data(students, courses)
    df['Grade'] = np.random.random(len(df['Grade']))*100
    students, courses = adjusted_gpa.additive_method(df)
    np.testing.assert_almost_equal(courses['c1'], 0)


def test_additive_scaling():
    df = pd.read_csv('caulkins.csv')
    students, courses = adjusted_gpa.additive_method(df)
    unscaled_fit = adjusted_gpa.check_additive_fit(df, students, courses)

    scaled_students, scaled_courses = adjusted_gpa.scale_additive_results(students, courses, 'Class #9', 0)
    nose.tools.eq_(scaled_courses['Class #9'], 0)
    scaled_fit = adjusted_gpa.check_additive_fit(df, scaled_students, scaled_courses)
    np.testing.assert_almost_equal(unscaled_fit, scaled_fit)

    scaled_students, scaled_courses = adjusted_gpa.scale_additive_results(students, courses, 'Class #8', 100)
    nose.tools.eq_(scaled_courses['Class #8'], 100)
    scaled_fit = adjusted_gpa.check_additive_fit(df, scaled_students, scaled_courses)
    np.testing.assert_almost_equal(unscaled_fit, scaled_fit)

    scaled_students, scaled_courses = adjusted_gpa.scale_additive_results(students, courses, 'Student A', 100)
    nose.tools.eq_(scaled_students['Student A'], 100)
    scaled_fit = adjusted_gpa.check_additive_fit(df, scaled_students, scaled_courses)
    np.testing.assert_almost_equal(unscaled_fit, scaled_fit)

    scaled_students, scaled_courses = adjusted_gpa.scale_additive_results(students, courses, 'Student D', 0)
    nose.tools.eq_(scaled_students['Student D'], 0)
    scaled_fit = adjusted_gpa.check_additive_fit(df, scaled_students, scaled_courses)
    np.testing.assert_almost_equal(unscaled_fit, scaled_fit)


def test_multiplicative_func():
    # two students, in two courses
    x = np.array([95, 80, 1.1, 0.9])
    y = np.array([90, 80, 75, 85])
    s_c = np.array([[0, 2],
                    [0, 3],
                    [1, 2],
                    [1, 3]])
    expected_result = np.array([90-(95/1.1), 80-(95/0.9), 75-(80/1.1), 85-(80/0.9)])
    result = adjusted_gpa.multiplicative_func(x, y, s_c)
    np.testing.assert_array_almost_equal(result, expected_result)


def test_make_multiplicative_indicator_matrix_and_grades():
    df = pd.read_csv('caulkins.csv')

    initial_guess, grades, students_and_courses, students, courses = adjusted_gpa.make_multiplicative_indicator_matrix_and_grades(df)

    nose.tools.eq_(len(initial_guess), 13)  # 13 unknowns
    nose.tools.eq_(len(students), 4)
    nose.tools.eq_(len(courses), 9)


def test_make_combined_indicator_matrix_and_grades():
    df = pd.read_csv('caulkins.csv')

    initial_guess, grades, students_and_courses, students, courses = adjusted_gpa.make_combined_indicator_matrix_and_grades(df)

    nose.tools.eq_(len(initial_guess), 22)
    nose.tools.eq_(len(students), 4)
    nose.tools.eq_(len(courses), 9)


def test_caulkins_multiplicative_solution():
    # Caulkins does not state exact adjusted GPA scores,
    # but that the adjusted rank ordering should be A>B>C>D (top of page 2)
    df = pd.read_csv('caulkins.csv')
    students, courses = adjusted_gpa.multiplicative_method(df)
    assert (students['Student A'] > students['Student B'] > students['Student C'] > students['Student D'])


def test_multiplicative_method():
    students = {'s1': 62, 's2': 79}
    courses = {'c1': 1, 'c2': 1.2}
    df, students_and_courses = adjusted_gpa.create_multiplicative_data(students, courses)
    result_students, result_courses = adjusted_gpa.multiplicative_method(df)
    result_students, result_courses = adjusted_gpa.scale_multiplicative_results(result_students, result_courses, 'c1', 1)
    validate_dicts(students, result_students)
    validate_dicts(courses, result_courses)

    # Three students in three courses, but every student
    # is missing from one of the courses
    students = {'s1': 62, 's2': 79, 's3': 81}
    courses = {'c1': 1, 'c2': 0.8, 'c3': 0.7}
    enrollments = [('s1', 'c2'), ('s1', 'c3'),
                   ('s2', 'c1'), ('s2', 'c3'),
                   ('s3', 'c1'), ('s3', 'c2')]
    df, students_and_courses = adjusted_gpa.create_multiplicative_data(students, courses, enrollments)
    result_students, result_courses = adjusted_gpa.multiplicative_method(df)
    result_students, result_courses = adjusted_gpa.scale_multiplicative_results(result_students, result_courses, 'c1', 1)
    validate_dicts(students, result_students)
    validate_dicts(courses, result_courses)


def test_multiplicative_method_noisy_data():

    # Three students in three courses
    students = {'s1': 62, 's2': 79, 's3': 81}
    courses = {'c1': 1, 'c2': 0.8, 'c3': 1.3}
    original_df, students_and_courses = adjusted_gpa.create_multiplicative_data(students, courses)

    # Add noise to the dataset
    noisy_df = original_df.copy()
    noisy_df.loc[(noisy_df['Student'] == 's1') & (noisy_df['Course'] == 'c1'), 'Grade'] += 3
    noisy_df.loc[(noisy_df['Student'] == 's2') & (noisy_df['Course'] == 'c3'), 'Grade'] -= 4

    # fit model parameters to noisy data
    students, courses = adjusted_gpa.multiplicative_method(noisy_df)
    new_df, _ = adjusted_gpa.create_multiplicative_data(students, courses)

#    np.testing.assert_almost_equal(courses['c1'], 1)

    # The RMSE between the noisy_df and the new_df should be smaller than between the noisy_df and original_df
    # in other words, the newly chosen model parameters should create a better fit to the data+noise than the original model parameters.
    RMSE_original = adjusted_gpa.RMSE(original_df['Grade'], noisy_df['Grade'])
    RMSE_new = adjusted_gpa.RMSE(new_df['Grade'], noisy_df['Grade'])
    nose.tools.ok_(RMSE_new <= RMSE_original)


def test_multiplicative_scaling():
    df = pd.read_csv('caulkins.csv')
    students, courses = adjusted_gpa.multiplicative_method(df)
    unscaled_fit = adjusted_gpa.check_multiplicative_fit(df, students, courses)

    scaled_students, scaled_courses = adjusted_gpa.scale_multiplicative_results(students, courses, 'Class #9', 1)
    nose.tools.eq_(scaled_courses['Class #9'], 1)
    scaled_fit = adjusted_gpa.check_multiplicative_fit(df, scaled_students, scaled_courses)
    np.testing.assert_almost_equal(unscaled_fit, scaled_fit)

    scaled_students, scaled_courses = adjusted_gpa.scale_multiplicative_results(students, courses, 'Class #8', 10)
    nose.tools.eq_(scaled_courses['Class #8'], 10)
    scaled_fit = adjusted_gpa.check_multiplicative_fit(df, scaled_students, scaled_courses)
    np.testing.assert_almost_equal(unscaled_fit, scaled_fit)

    scaled_students, scaled_courses = adjusted_gpa.scale_multiplicative_results(students, courses, 'Student A', 1)
    np.testing.assert_almost_equal(scaled_students['Student A'], 1)
    scaled_fit = adjusted_gpa.check_multiplicative_fit(df, scaled_students, scaled_courses)
    np.testing.assert_almost_equal(unscaled_fit, scaled_fit)

    scaled_students, scaled_courses = adjusted_gpa.scale_multiplicative_results(students, courses, 'Student D', 10)
    np.testing.assert_almost_equal(scaled_students['Student D'], 10)
    scaled_fit = adjusted_gpa.check_multiplicative_fit(df, scaled_students, scaled_courses)
    np.testing.assert_almost_equal(unscaled_fit, scaled_fit)


def test_combined_func():
    # two students, in two courses
    x = np.array([95, 80, 2, -3, 1.1, 0.9])
    y = np.array([90, 80, 75, 85])
    s_c = np.array([[0, 2, 4],
                    [0, 3, 5],
                    [1, 2, 4],
                    [1, 3, 5]])
    expected_result = np.array([90-((95/1.1)-2), 80-((95/0.9)+3), 75-((80/1.1)-2), 85-((80/0.9)+3)])
    result = adjusted_gpa.combined_func(x, y, s_c)
    np.testing.assert_array_almost_equal(result, expected_result)


def test_caulkins_combined_solution():
    # Caulkins does not state exact adjusted GPA scores,
    # but that the adjusted rank ordering should be A>B>C>D (top of page 2)

    df = pd.read_csv('caulkins.csv')
    students, courses = adjusted_gpa.combined_method(df)
    assert (students['Student A'] > students['Student B'] > students['Student C'] > students['Student D'])


def test_combined_method():
    students = {'s1': 70, 's2': 80, 's3': 90, 's4': 85, 's5': 92}
    courses = {'c1': {'a': 0, 'm': 1}, 'c2': {'a': -10, 'm': 1.1}, 'c3': {'a': 4, 'm': 0.9}}
    df, all_factors = adjusted_gpa.create_combined_data(students, courses)
    new_students, new_courses = adjusted_gpa.combined_method(df)
    new_students, new_courses = adjusted_gpa.scale_combined_results(new_students, new_courses, 'c1', {'a': 0, 'm': 1})
    validate_dicts(students, new_students)
    validate_combined_method_course_dicts(courses, new_courses)

    # six students in three courses, but every student
    # is missing from one of the courses
    students = {'s1': 62, 's2': 79, 's3': 81, 's4': 85, 's5': 92, 's6': 87}
    courses = {'c1': {'a': 0, 'm': 1}, 'c2': {'a': -10, 'm': 1.1}, 'c3': {'a': 4, 'm': 0.9}}
    enrollments = [('s1', 'c2'), ('s1', 'c3'),
                   ('s2', 'c3'), ('s2', 'c1'),
                   ('s3', 'c1'), ('s3', 'c2'),
                   ('s4', 'c2'), ('s4', 'c3'),
                   ('s5', 'c3'), ('s5', 'c1'),
                   ('s6', 'c1'), ('s6', 'c2')]
    df, students_and_courses = adjusted_gpa.create_combined_data(students, courses, enrollments)
    df, all_factors = adjusted_gpa.create_combined_data(students, courses)
    new_students, new_courses = adjusted_gpa.combined_method(df)
    new_students, new_courses = adjusted_gpa.scale_combined_results(new_students, new_courses, 'c1', {'a': 0, 'm': 1})
    validate_dicts(students, new_students)
    validate_combined_method_course_dicts(courses, new_courses)


def test_combined_method_noisy_data():

    students = {'s1': 70, 's2': 80, 's3': 90, 's4': 85, 's5': 92}
    courses = {'c1': {'a': 0, 'm': 1}, 'c2': {'a': -10, 'm': 1.1}, 'c3': {'a': 4, 'm': 0.9}}
    original_df, all_factors = adjusted_gpa.create_combined_data(students, courses)

    # Add noise to the dataset
    noisy_df = original_df.copy()
    noisy_df.loc[(noisy_df['Student'] == 's1') & (noisy_df['Course'] == 'c1'), 'Grade'] += 3
    noisy_df.loc[(noisy_df['Student'] == 's2') & (noisy_df['Course'] == 'c3'), 'Grade'] -= 4
    new_students, new_courses = adjusted_gpa.combined_method(noisy_df)
    new_students, new_courses = adjusted_gpa.scale_combined_results(new_students, new_courses, 'c1', {'a': 0, 'm': 1})
    new_df, _ = adjusted_gpa.create_combined_data(new_students, new_courses)

    np.testing.assert_almost_equal(new_courses['c1']['a'], 0)
    np.testing.assert_almost_equal(new_courses['c1']['m'], 1)

    # The RMSE between the noisy_df and the new_df should be smaller than between the noisy_df and original_df
    RMSE_original = adjusted_gpa.RMSE(original_df['Grade'], noisy_df['Grade'])
    RMSE_new = adjusted_gpa.RMSE(new_df['Grade'], noisy_df['Grade'])
    nose.tools.ok_(RMSE_new <= RMSE_original)


def test_combined_scaling():
    students = {'s1': 70, 's2': 80, 's3': 90, 's4': 85, 's5': 92}
    courses = {'c1': {'a': 0, 'm': 1}, 'c2': {'a': -10, 'm': 1.1}, 'c3': {'a': 4, 'm': 0.9}}
    df, all_factors = adjusted_gpa.create_combined_data(students, courses)
    students, courses = adjusted_gpa.combined_method(df)
    unscaled_fit = adjusted_gpa.check_combined_fit(df, students, courses)
    scaled_students, scaled_courses = adjusted_gpa.scale_combined_results(students, courses, 'c3', {'a': 0, 'm': 1})

    validate_dicts(scaled_courses['c3'], {'a': 0, 'm': 1})
    scaled_fit = adjusted_gpa.check_combined_fit(df, scaled_students, scaled_courses)
    np.testing.assert_almost_equal(scaled_fit, unscaled_fit)

    scaled_students, scaled_courses = adjusted_gpa.scale_combined_results(students, courses, 'c2', {'a': -5, 'm': 10})
    validate_dicts(scaled_courses['c2'], {'a': -5, 'm': 10})
    scaled_fit = adjusted_gpa.check_combined_fit(df, scaled_students, scaled_courses)
    np.testing.assert_almost_equal(scaled_fit, unscaled_fit)


def test_check_connectedness():
    students = {'s1': 62, 's2': 79}
    courses = {'c1': 0, 'c2': -10}
    # each student is enrolled in only one course each.
    df, all_factors = adjusted_gpa.create_additive_data(students, courses, enrollments=[('s1', 'c1'), ('s2', 'c2')])
    nose.tools.assert_raises(RuntimeError, adjusted_gpa.check_connectedness, df)

    # two students, both enrolled in both courses.
    df, all_factors = adjusted_gpa.create_additive_data(students, courses)
    adjusted_gpa.check_connectedness(df)
